#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "header.h"

void checkingTheStorage(char* storageFruit, unsigned int* inStorage) {

	FILE* checkStorage = fopen(storageFruit, "rb");
	if (checkStorage == NULL) {
		perror("Storage file doesn't exist on the first execution of the program\n");
		checkStorage = fopen(storageFruit, "wb");
		if (checkStorage == NULL) {
			perror("Storage file cannot be created\n");
			exit(EXIT_FAILURE);
		}
		else {
			fwrite(inStorage, sizeof(unsigned int), 1, checkStorage);
			fclose(checkStorage);
			printf("Storage file is created successfully\n");
		}
	}
	else {
		fread(inStorage, sizeof(unsigned int), 1, checkStorage);
		fclose(checkStorage);
		printf("Storage file already exists\n");
	}
}

void exitTheStorage(void) {
	printf("\nAre you sure you want to exit?[Yes/No]\n");
	char yORn[4] = { '\0' };
	scanf(" %3s", yORn);
	if (!strcmp("Yes", yORn)) {
		exit(EXIT_FAILURE);
	}
}

void addNewFruit(char* storageFruit, unsigned int* inStorage) {
	FILE* addNewFruitToStorage = NULL;
	addNewFruitToStorage = fopen(storageFruit, "rb+");

	if (addNewFruitToStorage == NULL) {
		perror("\nError opening the file while adding new fruit");
		return;
	}
	else {
		FRUIT tempCondition = { 0 };
		printf("\nEnter the name of the new fruit:\n");
		scanf(" %14s",tempCondition.name);
		printf("\nEnter the price of the fruit you want to add (per kg):\n");
		scanf(" %f", &tempCondition.price);
		printf("\nPlease enter the amount you want to add of that certain fruit:\n");
		scanf(" %d", &tempCondition.amountInStorage);
		tempCondition.numberOnScale = ((*inStorage)++)+1;

		fseek(addNewFruitToStorage, sizeof(unsigned int) + ((*inStorage - 1) * sizeof(FRUIT)), SEEK_SET);
		fwrite(&tempCondition, sizeof(FRUIT), 1, addNewFruitToStorage);
		rewind(addNewFruitToStorage);
		fwrite(inStorage, sizeof(unsigned int), 1, addNewFruitToStorage);
		fclose(addNewFruitToStorage);
		printf("\nFruit moved to storage successfully\n");
		return;
	}
}

void seeEverythingInStorage(char* storageFruit, unsigned int* inStorage) {
	unsigned int i;
	FILE* readTheContents = NULL;
	readTheContents = fopen(storageFruit, "rb");

	if (readTheContents == NULL) {
		perror("Reading the file");
		return;
	}
	else {
		FRUIT* allTheFruit = NULL;
		fread(inStorage, sizeof(unsigned int), 1, readTheContents);
		if (*inStorage == 0) {
			printf("\nThere is no fruit in storage\n");
			return;
		}
		else {
			allTheFruit = (FRUIT*)calloc(*inStorage, sizeof(FRUIT));
			if (allTheFruit == NULL) {
				perror("Reading the storage");
				fclose(readTheContents);
				return;
			}
			else {
				fread(allTheFruit, sizeof(FRUIT), *inStorage, readTheContents);
				fclose(readTheContents);

				for (i = 0; i < *inStorage; i++) {
					printf("\n%s", (allTheFruit + i)->name);
					printf("\n%d", (allTheFruit + i)->numberOnScale);
					printf("\n%f kn per kg", (allTheFruit + i)->price);
					printf("\n%d\n\n", (allTheFruit + i)->amountInStorage);
				}
				free(allTheFruit);
			}
		}
	}
}

void readCertainFruit(char* storageFruit, unsigned int* inStorage) {
	FILE* searchingByName = NULL;
	searchingByName = fopen(storageFruit, "rb");
	if (searchingByName == NULL) {
		perror("Cannot search");
		return;
	}
	else {
		FRUIT* MenuFruit = NULL;
		fread(inStorage, sizeof(unsigned int), 1, searchingByName);
		if (*inStorage == 0) {
			printf("There is no fruit to search");
			return;
		}
		else {
			MenuFruit = (FRUIT*)calloc(*inStorage, sizeof(FRUIT));
			if (MenuFruit == NULL) {
				perror("Reading the file");
				return;
			}
			else {
				fread(MenuFruit, sizeof(FRUIT), *inStorage, searchingByName);
				fclose(searchingByName);
				unsigned int i;
				printf("Please enter the name of the fruit(Proper capitalization): ");
				char tempFruit[16] = { '\0' };
				scanf("%15s", tempFruit);
				unsigned int flag = 0;
				unsigned int indFlag = -1;
				for (i = 0; i < *inStorage; i++) {
					if (!strcmp((MenuFruit + i)->name, tempFruit)) {
						flag = 1;
						indFlag = i;
					}
				}
				if (flag == 1) {
					printf("\n%s", (MenuFruit + indFlag)->name);
					printf("\n%d", (MenuFruit + indFlag)->numberOnScale);
					printf("\n%f kn per kg", (MenuFruit + indFlag)->price);
					printf("\n%d\n\n", (MenuFruit + indFlag)->amountInStorage);
				}
			}
		}
	}
}

void sort(char* storageFruit, unsigned int* inStorage) {
	unsigned int i;
	FILE* sortingByAmount = NULL;
	sortingByAmount = fopen(storageFruit, "rb");
	if (sortingByAmount == NULL) {
		perror("Sort");
		return;
	}
	else {
		FRUIT* sorting = NULL;
		fread(inStorage, sizeof(unsigned int), 1, sortingByAmount);
		if (*inStorage == 0) {
			printf("There is no fruit to sort");
			return;
		}
		else {
			sorting = (FRUIT*)calloc(*inStorage, sizeof(FRUIT));
			if (sorting == NULL) {
				perror("Reading the file");
				return;
			}
			else {
				int array[100] = { 0 };
				int helper;
				fread(sorting, sizeof(FRUIT), *inStorage, sortingByAmount);
				fclose(sortingByAmount);
				for (i = 0; i < *inStorage; i++) {
					array[i] = (sorting + i)->amountInStorage;
				}
				for (int j = 0; j < *inStorage; j++) {
					for (i = 0; i < *inStorage; i++) {
						if (array[i] > array[i + 1]) {
							helper = array[i];
							array[i] = array[i + 1];
							array[i + 1] = helper;
						}
					}
				}
				for (int j = 1; j < *inStorage + 1; j++) {
					for (i = 0; i < *inStorage + 1; i++) {
						if (array[j] == (sorting + i)->amountInStorage) {
							printf("\n%s", (sorting + i)->name);
							printf("\n%d", (sorting + i)->numberOnScale);
							printf("\n%f kn per kg", (sorting + i)->price);
							printf("\n%d\n\n", (sorting + i)->amountInStorage);
							if (array[j + 1] == 0) {
								break;
							}
						}
					}
				}
				free(sorting);
			}
		}
	}
}


void menu(char* storageFruit, unsigned int* inStorage) {
	while (1) {
		int decision;
		printf("\nSTORAGE\n\n1.Add a new fruit to the storage\n2.Look for the certain fruit in the storage\n3.See the whole list\n4.Sort by the amount in storage\n5.Exit\n");
		scanf(" %d", &decision);
		if (decision == 1) {
			addNewFruit(storageFruit, &inStorage);
		}
		if (decision == 2) {
			readCertainFruit(storageFruit, &inStorage);
		}
		if (decision == 3) {
			seeEverythingInStorage(storageFruit, &inStorage);
		}
		if (decision == 4) {
			sort(storageFruit, &inStorage);
		}
		if (decision == 5) {
			exitTheStorage();
		}
		else {
			printf("\nYour input wasn't in the selection");
		}
	}
}


#ifndef HEADER_H
#define HEADER_H

typedef struct fruit {
	char name[15];
	int numberOnScale;
	float price;
	int amountInStorage;
}FRUIT;

void addNewFruit(char*, unsigned int* );
void exitTheStorage(void);
void checkingTheStorage(char*, unsigned int*);
void menu(char* , unsigned int*);
void seeEverythingInStorage(char*, unsigned int*);
void readCertainFruit(char*, unsigned int*);
void sort(char* , unsigned int*);
#endif